import { EventData } from "tns-core-modules/data/observable";
import { Page } from "tns-core-modules/ui/page";
import { HomeModel } from "./home-model";

export function navigatingTo(args: EventData) {
    const page = <Page>args.object;
    page.bindingContext = new HomeModel(page);
}