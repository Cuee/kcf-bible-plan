import { Observable } from "tns-core-modules/data/observable";
import { request, getFile, getImage, getJSON, getString } from "tns-core-modules/http";
import { PARAMETERS } from "../config/params";
import { SelectedPageService } from "../shared/selected-page-service";
import * as appSettings from "tns-core-modules/application-settings";
import { Page } from "tns-core-modules/ui/page/page";
import { Label } from "tns-core-modules/ui/label/label";
import { ListView } from "tns-core-modules/ui/list-view/list-view";

export class BibleViewModel extends Observable {
    private _page: Page;
    private apiKey: string;
    constructor(page) {
        super();
        this._page = page;
        this.apiKey = PARAMETERS["api-key"];
        SelectedPageService.getInstance().updateSelectedPage("Bible");
        this.sendBibleRequest("", "GET")
            .then((response) => {
                const result = response.content.toJSON();
                this.saveBibleVersions(result);
                this.loadBiblePageTitle();

            },
                (error) => {
                    alert(error)
                    console.log(error);
                });

        let bibleVersionId = appSettings.getString("KJV-ID", "");
        this.sendBibleRequest(bibleVersionId + "/books", "GET")
            .then((response) => {
                const result = response.content.toJSON();
                this.saveBibleBooks(result);
            },
                (error) => {
                    alert(error)
                    console.log(error);
                });
        let bibleBooks = JSON.parse(appSettings.getString("bible-books", "[]"));
        bibleBooks.map((bibleBooks) => {
            this.sendBibleRequest(bibleVersionId + "/books/" + bibleBooks.id + "/chapters", "GET")
                .then((response) => {
                    const result = response.content.toJSON();
                    this.saveBookChapters(result);
                    let chapters = JSON.parse(appSettings.getString("book-chapters", "[]"));
                    chapters.map((chapterInput) => {
                        this.sendBibleRequest(bibleVersionId + "/chapters/" + chapterInput.id + "/verses", "GET")
                            .then((verseResponse) => {
                                const verseResult = verseResponse.content.toJSON();
                                this.saveVerses(verseResult);
                                let verses = JSON.parse(appSettings.getString("verses", "[]"));
                                verses.map((verseInput) => {
                                    this.sendBibleRequest(bibleVersionId + "/verses/" + verseInput.id + "?include-chapter-numbers=false&include-verse-numbers=false", "GET")
                                        .then((verseContent) => {
                                            const content = verseContent.content.toJSON();
                                            this.saveContent(content);
                                        },
                                            (error) => {
                                                alert(error)
                                                console.log(error);
                                            });
                                })
                            },
                                (error) => {
                                    alert(error)
                                    console.log(error);
                                });
                    })
                },
                    (error) => {
                        alert(error)
                        console.log(error);
                    });
        })
    }

    saveBibleVersions(result) {
        const versions = result.data.map((data) => {
            return {
                name: data.name,
                id: data.id,
                abbreviation: data.abbreviation,
                description: data.description,
                language: data.language.name
            };
        });
        appSettings.setString("bible-versions", JSON.stringify(versions));
    }

    saveBibleBooks(result) {
        const books = result.data.map((data) => {
            return {
                id: data.id,
                bibleId: data.bibleId,
                abbreviation: data.abbreviation,
                name: data.name,
                nameLong: data.nameLong
            };
        });
        appSettings.setString("bible-books", JSON.stringify(books));
        let listView = <ListView>this._page.getViewById("bibleBooks");
        listView.items = books;
    }

    saveBookChapters(result) {
        const chapters = result.data.map((data) => {
            return {
                number: data.number,
                id: data.id
            };
        });
        appSettings.setString("book-chapters", JSON.stringify(chapters));

    }

    saveVerses(result) {
        const verses = result.data.map((data) => {
            return {
                id: data.id
            };
        });
        appSettings.setString("verses", JSON.stringify(verses));
    }

    saveContent(result){
        console.dir(result)
        const content = result.data.map((data) => {
            return {
                refrence: data.refrence,
                content: data.content
            };
        });
        appSettings.setString("verses", JSON.stringify(content));
    }

    loadBiblePageTitle() {
        let apiBibleVersions = JSON.parse(appSettings.getString("bible-versions", "[]"));
        let bibleLabel = <Label>this._page.getViewById("bibleVersions");
        bibleLabel.text = apiBibleVersions[18].abbreviation.toUpperCase();
        appSettings.setString("KJV-ID", apiBibleVersions[18].id);
    }

    sendBibleRequest(endpoint, requestMethod) {
        return request({
            url: "https://api.scripture.api.bible/v1/bibles" + ((endpoint === "") ? "" : ("/" + endpoint)),
            method: requestMethod,
            headers: {
                "Content-Type": "application/json",
                "Connection": "Keep-Alive",
                'Accept': 'application/json',
                'api-key': this.apiKey

            },
        })

    }


}


