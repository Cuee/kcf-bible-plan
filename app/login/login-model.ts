import { Observable } from "tns-core-modules/data/observable";
import { topmost } from "tns-core-modules/ui/frame/frame";


export class LoginModel extends Observable {
    constructor(page) {
        super();
        page.actionBarHidden = true;
    }

    login() {
        topmost().navigate("home/home");
    }

}
